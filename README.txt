# Voronoi Insertion

This folder contains tools to demonstrate the algorithm described in the paper.

The source code is in main.cpp.

A point set generated from census data has been provided in orig_inputs/illinois_cities.txt.

A demo script that creates and runs several examples is provided in demo.sh

#The script

The script can be run in the usual way:

'''bash
sh demo.sh
'''

The script first compiles the code with the provided makefile, and sets up input and output directories for the code to use.

After compiling to create an executable, the script creates all txt files that will be used for input and stores them in "inputs".

The input files created are 6 sparsifications of the Illinois point set (provided separately in the folder orig_inputs), 5 hex meshes, and 11 random meshes.

The meshes created are progressively larger and the total time it takes to construct and run the algorithm on them scales as O(n log n).

The hex inputs are labelled by the number of layers in the mesh, but there are actually roughly n^2 points in a hex mesh of n layers.

We've added output messages at every stage to explain what is going on at every point of execution. Occasionally, CGAl precondition violations may be raised and outputted to the screen. The violations are not unexpected behavior and the algorithm knows how to correctly handle them, and continues past them.

At the end of execution we output four pdf files with relevant Voronoi diagrams and one txt file of stats for the run. 



#Usage

#Input file commands

The executable may be used to generate txt files that contain coordinates of meshes that are hexagonal, randomly sampled, or a sparsification of a point set in a different txt file.

The way to use these commands is:

1. Generating a hex mesh:

'''bash
./voronoi_insertion write_hex_mesh [filename] [n]
'''

This writes a hexagonal mesh of "n" points per row (and O(n) rows, for a total of O(n^2) points) into "filename".

2. Generating a random mesh:

'''bash
./voronoi_insertion write_random [filename] [n]
'''

This writes a random mesh of "n" points into "filename".


3. Sparsifying a point set:

'''bash
./voronoi_insertion sparsify [input filename] [output filename]
'''

This reads points from the input file and randomly includes each point in the output with a 50% chance. This is often useful to run the algorithm faster on larger point sets to get a general sense of the results, as much of the structure is preserved. 


#Notes about Input file format/using geographical data:

Of course, the user may wish to use real world data/custom data with the provided program. All one needs to do is to scale all points involved to fit into a 2500x2500 coordinate space, with both coordinates non-negative. The origin is at the top left, the x axis increases from left to right and the y axis increases from top to bottom.

#Notes about the Illinois points

The points representing census data from Illinois in the file orin_inputs/illinois_cities.txt are inverted by 180 degrees. This is because our algorithm picks a point on the lower left of the point set to start, and the region we were interested in could be brought to the lower left only when rotated by 180 degrees. We rotated the outputs back to their usual orientations when presenting the data in the paper. Typically all pdf viewers have an option to rotate by 90 degrees and two uses of that will have the points in the right configuration.


#Running the main algorithm and outputs to expect:

The other way to pass arguments to the executable is the following: 

'''bash
./voronoi_insertion [input file path] [output file path]
'''

This runs the main algorithm on the points in the input file (assumed to be valid).

The algorithm finds a group of points in the interior of the point set and picks the lower leftmost point to be the start, and the furthest point from the start within the inner points to be the end. The exact details are of course documented in the code in the functions point_set_compute_inner_points, point_set_compute_llmost, and point_set_compute_furthest. The choices made in these functions are completely arbitrary (for demo purposes) and may be overridden to any desired start and end points (whose coordinates are known) in the algorithm::run() function by setting them both to ag.nearest_neighbor(custom_point) in the two lines of code that initialize the vertex handles called v and w. Here "custom_point" are the coordinates to set either the start or end to. Nothing else needs to be changed.

(In particular, for the illinois point set the choice of parameters 0.25, 0.6, 0.25, 0.7 in the call to point_set_compute_inner_points() in algorithm::run() might be more suitable to ensure that the algorithm never uses the concave pockets at the boundaries of the point set and always stays in the diagram. The default set is 0.15, 0.8, 0.15, 0.8, which picks points closer to the edges and may cause the algorithm to avoid the interior for efficiency.)

After the choice of starts and ends, the algorithm runs BFS to determine a naive way to connect the two cells. The BFS distance is noted and outputted for reference.

After this, the core algorithm is run by modifying an underlying additive Voronoi diagram as described in the paper.

Once the target cell is reached, an optimal insertion sequence is computed and inserted into the unweighted Voronoi diagram of the input points. 

Finally, four pdf files are generated: the input Voronoi diagram, the Voronoi diagram with a bfs path coloured, the Voronoi diagram with an optimal path coloured, and an additive Voronoi diagram of all the disks inserted. A txt file is also created storing stats from the run.
