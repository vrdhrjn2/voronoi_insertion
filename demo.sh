#!/bin/bash


make clean
make

echo "Setting up output directories"

rm -r -f output/ inputs/ >& /dev/null
mkdir -p "output"
mkdir -p "inputs"
#mkdir -p "output/hex"
#mkdir -p "output/rand"
#mkdir -p "output/custom"


### Generating inputs


./voronoi_insertion sparsify orig_inputs/illinois_cities.txt \
                    inputs/illinois_cities_d_02.txt
./voronoi_insertion sparsify inputs/illinois_cities_d_02.txt \
                    inputs/illinois_cities_d_04.txt
./voronoi_insertion sparsify inputs/illinois_cities_d_04.txt \
                    inputs/illinois_cities_d_08.txt
./voronoi_insertion sparsify inputs/illinois_cities_d_08.txt \
                    inputs/illinois_cities_d_16.txt
./voronoi_insertion sparsify inputs/illinois_cities_d_16.txt \
                    inputs/illinois_cities_d_32.txt
./voronoi_insertion sparsify inputs/illinois_cities_d_32.txt \
                    inputs/illinois_cities_d_64.txt


echo "Generating a hex mesh of 10 layers and computing path"
./voronoi_insertion write_hex_mesh inputs/hex_010.txt 10


echo "Generating a hex mesh of 30 layers and computing path"
./voronoi_insertion write_hex_mesh inputs/hex_030.txt 30

echo "Generating a hex mesh of 60 layers and computing path"
./voronoi_insertion write_hex_mesh inputs/hex_060.txt 60

echo "Generating a hex mesh of 90 layers and computing path"
./voronoi_insertion write_hex_mesh inputs/hex_090.txt 90

echo "Generating a hex mesh of 120 layers and computing path"
./voronoi_insertion write_hex_mesh inputs/hex_120.txt 120


echo "Generating a random mesh of 100 points and computing path"
./voronoi_insertion write_random inputs/rand_00100.txt 100

echo "Generating a random mesh of 200 points and computing path"
./voronoi_insertion write_random inputs/rand_00200.txt 200

echo "Generating a random mesh of 400 points and computing path"
./voronoi_insertion write_random inputs/rand_00400.txt 400


echo "Generating a random mesh of 1000 points and computing path"
./voronoi_insertion write_random inputs/rand_01000.txt 1000

echo "Generating a random mesh of 2000 points and computing path"
./voronoi_insertion write_random inputs/rand_02000.txt 2000

echo "Generating a random mesh of 4000 points and computing path"
./voronoi_insertion write_random inputs/rand_04000.txt 4000

echo "Generating a random mesh of 8000 points and computing path"
./voronoi_insertion write_random inputs/rand_08000.txt 8000

echo "Generating a random mesh of 8000 points and computing path"
./voronoi_insertion write_random inputs/rand_16000.txt 16000

echo "Generating a random mesh of 32000 points and computing path"
./voronoi_insertion write_random inputs/rand_32000.txt 32000

echo "Generating a random mesh of 64000 points and computing path"
./voronoi_insertion write_random inputs/rand_64000.txt 64000

echo "Generating a random mesh of 128000 points and computing path"
./voronoi_insertion write_random inputs/rand_128000.txt 128000

##################################################################
##################################################################
# Running the program...
##################################################################
##################################################################

## Hex
./voronoi_insertion inputs/hex_010.txt     output/hex_010
./voronoi_insertion inputs/hex_030.txt     output/hex_030
./voronoi_insertion inputs/hex_060.txt     output/hex_060
./voronoi_insertion inputs/hex_090.txt     output/hex_090
./voronoi_insertion inputs/hex_120.txt    output/hex_120


############### Illinois

#echo "Computing a path using provided illinois points"#
#./voronoi_insertion inputs/illinois_cities.txt output/illinois

echo "Computing a path using provided illinois points/8"
./voronoi_insertion inputs/illinois_cities_d_02.txt output/i_d_02

echo "Computing a path using provided illinois points/4"
./voronoi_insertion inputs/illinois_cities_d_04.txt output/i_d_04

echo "Computing a path using provided illinois points/8"
./voronoi_insertion inputs/illinois_cities_d_08.txt output/i_d_08

echo "Computing a path using provided illinois points/16"
./voronoi_insertion inputs/illinois_cities_d_16.txt output/i_d_16

echo "Computing a path using provided illinois points/32"
./voronoi_insertion inputs/illinois_cities_d_32.txt output/i_d_32

echo "Computing a path using provided illinois points/64"
./voronoi_insertion inputs/illinois_cities_d_64.txt output/i_d_64
                    



### Random
./voronoi_insertion inputs/rand_00100.txt  output/rand_00100
./voronoi_insertion inputs/rand_00200.txt  output/rand_00200
./voronoi_insertion inputs/rand_00400.txt  output/rand_00400
./voronoi_insertion inputs/rand_01000.txt  output/rand_01000
./voronoi_insertion inputs/rand_02000.txt  output/rand_02000
./voronoi_insertion inputs/rand_04000.txt  output/rand_04000

./voronoi_insertion inputs/rand_08000.txt  output/rand_08000
./voronoi_insertion inputs/rand_16000.txt  output/rand_16000
./voronoi_insertion inputs/rand_32000.txt  output/rand_32000
./voronoi_insertion inputs/rand_64000.txt  output/rand_64000
./voronoi_insertion inputs/rand_128000.txt  output/rand_128000

