all: voronoi_insertion


FLGA =  $(shell pkg-config --libs --cflags cairo cairo-svg)
FLGB =  $(shell pkg-config --libs --cflags gtk+-3.0)
DATE = $(shell datestr)

FLAGS = $(FLGA) $(FLGB) -lCGAL  -lCGAL_Core

voronoi_insertion: main.cpp
	g++  -Wall  -o voronoi_insertion main.cpp -L/usr/lib/x86_64-linux-gnu/ $(FLAGS)

kitit:
	zip voronoi_insertion_$(DATE) *.h *.C Makefile 

clean:
	rm -f *.o voronoi_insertion


