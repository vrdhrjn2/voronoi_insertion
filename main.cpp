///////////////////////////////////////////////////////////////////////////
//  main.cpp
//  voronoi_linking
//
//  Created by Rajgopal on 15/07/19.
//  Copyright � 2019 Rajgopal. All rights reserved.
//
// standard includes
///////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------
// Uncomment to use exact arithmetic.
//#define  EXACT_ART 1
//---------------------------------------------------------------------

#include  <chrono>
#include  <ctime>
#include  <iostream>
#include  <cmath>
#include  <set>
#include  <list>
#include  <sstream>
#include  <unordered_map>
#include  <queue>
#include  <stdlib.h>
#include  <time.h>
#include  <fstream>
#include  <cassert>
#include  "cairo.h"
//#include <cairo-pdf.h>
//#include  <png.h>

#include  <string>
#include  <vector>
#include  <iterator>
#include  <stdio.h>
#include  <stdint.h>
#include  <string.h>
#include  <errno.h>
#include  <algorithm>
#include  <png.h>
#include  <locale.h>

#include  "cairo-pdf.h"

#include  <CGAL/Bbox_2.h>



#ifdef EXACT_ART
#include <CGAL/Mpzf.h>
#include <CGAL/MP_Float.h>
#endif


///////////////////////////////////////////////////////////////////
// Types
// typedef for the traits; the filtered traits class is used

#ifdef  EXACT_ART
//typedef CGAL::MP_Float real;
#include  <CGAL/Exact_algebraic.h>
typedef CGAL::Exact_algebraic real;
//  typedef CGAL::Mpzf real;
#else
typedef long double  real;
#endif
///////////////////////////////////////////////////////////////////

#include  <CGAL/Simple_cartesian.h>

#include <CGAL/Apollonius_graph_2.h>
#include <CGAL/Apollonius_graph_traits_2.h>

#include  <CGAL/Apollonius_graph_2.h>
#include  <CGAL/Triangulation_data_structure_2.h>
#include  <CGAL/Apollonius_graph_vertex_base_2.h>
#include  <CGAL/Triangulation_face_base_2.h>


typedef CGAL::Simple_cartesian<real> Rep;
#ifdef EXACT_ART
#include  <CGAL/Apollonius_graph_traits_2.h>
typedef CGAL::Apollonius_graph_traits_2<Rep> Traits;
#else
#include  <CGAL/Apollonius_graph_filtered_traits_2.h>
typedef CGAL::Apollonius_graph_filtered_traits_2<Rep> Traits;
#endif

//typedef CGAL::Apollonius_graph_2<Traits>          WVoronoi;
#include <CGAL/Apollonius_graph_hierarchy_2.h>
typedef  CGAL::Apollonius_graph_hierarchy_2<Traits> WVoronoi;

#define  GEN_INPUT       0
#define  GEN_BOUNDING   -2
#define  GEN_TARGET     -3


real  epsilon = 0.0000001; //threshold to display the circle around a weighted site
//int mode = 0; // variable to track if we are in a hex/random/custom mesh
// typedefs for the algorithm

typedef CGAL::Bbox_2 Box2d;
typedef  Rep::Point_2     Point;
typedef  Rep::Segment_2   Segment;
typedef  Traits::Site_2   Site;
typedef  std::list<Traits::Site_2>  SitesList;
typedef  std::list<Segment>  SegsList;
typedef  std::vector<Point>  PointSet;
typedef  WVoronoi::Vertex_handle     VertexH;
typedef  WVoronoi::Face_handle       FaceH;
typedef  WVoronoi::Edge              Edge;
typedef  WVoronoi::Vertex_circulator VertexCirc;
typedef  WVoronoi::Edge_circulator   EdgeCirc;
typedef  WVoronoi::Vertex_iterator   VerterIter;
typedef  WVoronoi::Face_circulator   FaceCirc;

typedef  std::vector<Site>  SitesVector;

//////////////////////////////////////////////////////////////////////////
// Bounding box functions
//////////////////////////////////////////////////////////////////////////

////////////////
real  dist_sq( const Point  & a, const Point  & b )
{
    real   dx, yx;
    
    dx = a.x() - b.x();
    yx = a.y() - b.y();

    return  dx*dx + yx*yx;
}

/**
 * @param box - bounding box to check within
 * @param p - point to check membership of
 * This function returns whether the point p lies inside the bounding box "box"
 */
inline bool  BBox_is_in( Box2d  & box, const Point  & p )
{
    if  ( ( p.x() < box.xmin() )
          ||  ( p.x() > box.xmax() )
          ||  ( p.y() < box.ymin() )
          ||  ( p.y() > box.ymax() ) )
        return  false;
    return  true;
}

//wrapper to CGAL casting function
double  td( const real  & r )
{
    return  CGAL::to_double( r );
}

/**
 * @param bb - Bounding box to expand
 * @param factor - factor by which to expand given box
 *
 * This function returns a box that has both x and y dimensions
 * enlarged by the given factor
 */
Box2d   BBox_expand( const Box2d  & bb, real  factor )
{
    real x_delta, y_delta;

    //Computing distance of midpoint from extremities
    x_delta = ( bb.xmax() - bb.xmin() ) / 2.0;
    y_delta = ( bb.ymax() - bb.ymin() ) / 2.0;

    //Computing coordinates of midpoint
    Point  c( (bb.xmax() + bb.xmin()) / 2.0,
              (bb.ymax() + bb.ymin()) / 2.0 );

    //Setting new extremities as being "factor" away from the midpoint
    //e.g. when factor = 1, the min and max values are the same as
    //provided box's so this scales both dimensions linearly by factor
    real  xmin, xmax, ymin, ymax;
    xmin = c.x() - x_delta * factor;
    xmax = c.x() + x_delta * factor;

    ymin = c.y() - y_delta * factor;
    ymax = c.y() + y_delta * factor;
    
    //returning box with new dimensions
    return  Box2d(  CGAL::to_double( xmin ), CGAL::to_double( ymin ),
                    CGAL::to_double( xmax ), CGAL::to_double( ymax ) );
}

/**
 * @param bx - box object to start with
 * @param p - point to include in new bounding box
 *
 * This function computes a bounding box that bounds an existing
 * bounding box and a point
 */
void  BBox_bound( Box2d  & bx, const Point  & p )
{
    //Point is already in bx
    if  ( BBox_is_in( bx, p ) )
        return;

    //New extremities to be computed
    real xmin = std::min( (real)bx.xmin(), p.x() );
    real xmax = std::max( (real)bx.xmax(), (real)p.x() );

    real ymin = std::min( (real)bx.ymin(), p.y() );
    real ymax = std::max( (real)bx.ymax(), p.y() );

    //Updating dimensions of bx
    bx = Box2d( td(xmin), td(ymin), td(xmax), td(ymax) );
}

/**
 * @param ps - point set to compute bounding box around
 *
 * This function computes the bounding box for a given set of points
 */
Box2d  ComputeBBox( PointSet  & ps )
{
    assert( ps.size() > 0 );

    real  xmin, xmax, ymin, ymax;

    xmin = xmax = ps[ 0 ].x();
    ymin = ymax = ps[ 0 ].y();
    
    //Finding the extremities of the point set
    for  ( auto  pnt : ps ) {
        xmin = std::min( pnt.x(), xmin );
        xmax = std::max( pnt.x(), xmax );

        ymin = std::min( pnt.y(), ymin );
        ymax = std::max( pnt.y(), ymax );
    }
    
    //creating a box object with the dimensions corresponding to extremities
    return  Box2d( td(xmin),  td(ymin), td( xmax ), td( ymax) );
}

//wrapper to division function
const real   divr( const real & x, const real & y)
{
    return  x / y;
}

//////////////////////////////////////////////////////////////////////
// Real random - start
//////////////////////////////////////////////////////////////////////

#ifndef  RAND_MAX
#define  RAND_MAX    0x7fffffff
#endif  // RAND_MAX

/**
 * @param out - variable into which to write a random real number
 * between 0 and 1
 *
 * This function computes a random real number between 0 and 1 and
 * writes it to "out"
 */
inline  void    realRand( real  & out )
{
    real  a( rand() );
    real  b( RAND_MAX );
    real  tmp;

    tmp = divr( a, b );

    //printf( "RAND: %g\n", td(tmp) );
    
    out = tmp;
}

//////////////////////////////////////////////////////////////////////////
// RGB
//////////////////////////////////////////////////////////////////////////
class  Color
{
public:
    double red, green, blue;

    Color( double _red, double _green, double  _blue ) {
        red = _red;
        green = _green;
        blue = _blue;
    }

    void  set_source_rgb( cairo_t  * cr ) {
        cairo_set_source_rgb( cr, red, green, blue );
    }
};

static Color  c_middle_cells_fill      ( 0.7, 1.0, 0.7 );
static Color  c_middle_sites_fill      ( 0.5, 0.0, 0.0 );
static Color  c_middle_sites_boundary  ( 0.0, 0.0, 0.0 );

static Color  c_src                    ( 0.5, 0.9, 0.0 );
static Color  c_trg                    ( 0.9, 0.9, 0.0 );

static Color  c_src_fill               ( 0.0, 0.9, 0.99 );
static Color  c_src_boundary           ( 0.0, 0.2, 0.0 );

static Color  c_trg_fill               ( 0.0, 0.9, 0.99 );
static Color  c_trg_boundary           ( 0.0, 0.2, 0.0 );


//////////////////////////////////////////////////////////////////////////
// Point set functions
//////////////////////////////////////////////////////////////////////////

/**
 * @param ps - point set into which to insert a new point
 * @param x - x coordinate of new point to be inserted
 * @param y - y coordinate of new point to be inserted
 *
 * This function inserts a new point at the coordinates x, y into the
 * pointset ps Points are actually perturbed by a small amount of
 * noise to avoid degeneracies
 */
void  p_insert( PointSet  & ps, const real  &x, const real & y )
{
    real  xn, yn;
    double  scale;

    //used to avoid degeneracies
    scale = 20.0; //scale factor for noise
    realRand( xn ); //initializing x to random real between 0 and 1
    realRand( yn ); //initializing y to random real between 0 and 1
    
    //adding point with noise
    ps.push_back( Point( x + scale * xn, y + scale * yn ) );
}


/**
 * @param ps - pointset into which to insert points along a segment
 * @param x1 - start of x coordinate of the segment
 * @param x2 - end of x coordinate of the segment
 * @param y1 - start of y coordinate of the segment
 * @param y2 - end of y coordinate of the segment
 * @param n - number of points to add
 *
 * This function inserts n equi-spaced points along the segment
 * from (x1,y1) to (x2, y2)
 */
void  p_insert_seg( PointSet  & ps,
                    const real  &x1, const real & y1,
                    const real  &x2, const real & y2,
                    int  n
                    )
{
    assert( n > 0 );
    //computing the spacing between the equi-spaced points
    real  intervals = (real)(n + 1);
    real  delta_x = divr( (x2 - x1), intervals );
    real  delta_y = divr( (y2 - y1), intervals );

    real p_x, p_y;

    //initializing first point to be added
    p_x = x1 + delta_x;
    p_y = y1 + delta_y;
    
    //adding n points using the spacing computed earlier
    for  ( int i = 0; i < n; i++ ) {
        p_insert( ps, p_x, p_y );
        p_x += delta_x;
        p_y += delta_y;
    }
}


/**
 * @param ps - point set to compute bounding points around
 * @param bb_in - bounding box containing given point set
 *
 * This function adds points around the bounding box
 * to make the diagram of interest only involving all finite sites
 */
void  point_set_add_voronoi_bounding_points( PointSet  & ps,
                                             const Box2d    & bb_in )
{
    Box2d   bb = BBox_expand( bb_in, 1.01 );

    real xm, xp, ym, yp;

    xm = bb.xmin();
    xp = bb.xmax();
    ym = bb.ymin();
    yp = bb.ymax();

    // Insert corners
    p_insert( ps, xm, ym );
    p_insert( ps, xm, yp );
    p_insert( ps, xp, ym );
    p_insert( ps, xp, yp );

    
    int  n = 31;
    //each call inserts n points in the specified intervals
    //each call handles one edge of the boundary
    p_insert_seg( ps,xm, ym,xm, yp, n );
    p_insert_seg( ps,xp, ym,xp, yp, n );
    p_insert_seg( ps,xm, ym,xp, ym, n );
    p_insert_seg( ps,xm, yp,xp, yp, n );
}


/**
 * @param ps - PointSet to compute the inner points of
 * @param qs - PointSet into which the inner points will be written
 * @param t_lx - affine ratio determining leftmost x coordinate of the
 *               inner rectangle
 * @param t_rx - affine ratio determining rightmost xcoordinate of the
 *               inner rectangle
 * @param t_ly - affine ratio determining highest y coordinate of the
 *               inner rectangle
 * @param t_ry - affine ratio determining lowest y coordinate of the
 *               inner rectangle
 *
 * This function computes a rectangle region within the pointset so
 * that the points chosen are sufficiently away from the edges of the
 * voronoi diagram Each parameter t_lx/y determines a ratio t : 1
 * between the line joining the min and max coordinates in a
 * particular dimension (x or y)
 *
 * In particular, the x range is a segment between t_lx and t_rx of
 * the way between min and max x-range of the pointset, and likewise
 * for the y range Choosing | t_lx - t_rx | ~ 0.3 - 0.5 gives us a
 * good demonstration of the algorithm without running outside the
 * diagram If it is desired to restrict points within a certain
 * boundary, a boundary function would need to be provided and the
 * validation function is_create_vertex would need to be modified By
 * default, choosing a box away from the edges works for understanding
 * the behavior of the algorithm
 */
void point_set_compute_inner_points( const PointSet  & ps,
                                        PointSet  & qs,
                                        real t_lx, real t_rx, real t_ly, real t_ry){
    printf("computing an interior set to avoid edges\n");
    
    //Computing the extreme x and y coordinates of the point set
    real  minx, maxx, miny,maxy;
    minx = miny = 10000.0;
    maxx = maxy = -1.0;
    for  ( auto  p : ps ) {
        real x,y;
        x = p.x();
        y = p.y();
        
        if(x < minx)
            minx = x;
        if(x > maxx)
            maxx = x;
        if(y < miny)
            miny = y;
        if(y > maxy)
            maxy = y;
    }
    
    //printf("bounds are minx:%Lf maxx:%Lf miny:%Lf maxy:%Lf\n",
    //minx,maxx,miny,maxy);
    
    //Computing affine combinations to get ranges in x and y
    real lx, rx, ly, ry;
    lx = (1-t_lx)*minx + t_lx*maxx;
    rx = (1-t_rx)*minx + t_rx*maxx;
    ly = (1-t_ly)*miny + t_ly*maxy;
    ry = (1-t_ry)*miny + t_ry*maxy;
    
    //printf("bounds are lx:%Lf rx:%Lf ly:%Lf ry:%Lf \n",lx,rx,ly,ry);
    
    //writing all points within the computed x and y range only into qs
    for  ( auto  p : ps ) {
        real x,y;
        x = p.x();
        y = p.y();
        if( x >= lx && x <= rx && y >=ly && y<=ry)
            qs.push_back(p);
    }
    printf("interior set computed!\n");
}

/**
 * @param ps - Point set in which to find the lower leftmost point
 *
 * This function iterates over the set and finds the lower leftmost
 * point in the region Meanings of lower and leftmost are in standard
 * cairo canvas coordinates
 */
Point   point_set_compute_llmost( const PointSet  & ps )
{
    assert( ps.size() > 0 );
    //printf("computing lower leftmost point in set\n");
    //printf( "ps.size: %d\n", (int)ps.size() );
    Point  curr = ps[ 0 ];
    int  count = 0;
    for  ( auto  p : ps ) {
        count++;
        //printf( "%d:  p:  (%g, %g)\n", count, (double)p.x(), (double)p.y() );
        //        if  ( (p.y() - p.x()) > (y - x)  ) {
        if  ( p.y()  > curr.y() )
            curr = p;
    }

    printf( "curr: (%g, %g)\n", (double)curr.x(), (double)curr.y() );
    return  curr;
}


/**
 * @param ps - provided set of points
 * @param ctr - point from which to compute furthest point
 *
 * This function finds the point in ps furthest away in Euclidean
 * distance from the point ctr
 */
Point   point_set_compute_furthest( const PointSet  & ps, Point ctr)
{
    assert( ps.size() > 0 );

    printf("Computing furthest\n");

    real d = -1;
    Point  p_curr;
    
    //Iterating over point set, finding the maximum distance away
    //and remembering coordinates of the furthest point
    for  ( auto  p : ps ) {
        real cand = dist_sq( p, ctr );
        if  ( cand > d ) {
            d = cand;
            p_curr = p;
        }
    }
    return  p_curr;// Point( x, y);
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//Class to extract segments, rays and lines from a given apollonius graph
//This is based on the CGAL 2D triangulation demo
//hyperlink to demo:
//
//https://doc.cgal.org/latest/Triangulation_2/
//          ... Triangulation_2_2print_cropped_voronoi_8cpp-example.html
//
//rays, lines and segments are stored in a list of segments an
//operator << is defined for this purpose
//////////////////////////////////////////////////////////////////////////

struct Cropped_voronoi_from_apollonius
{
    //segment list into which features will be written
    SegsList m_cropped_vd;

    //bounding box in which diagram is to be drawn
    Rep::Iso_rectangle_2 m_bbox;

    Cropped_voronoi_from_apollonius(const Rep::Iso_rectangle_2& bbox)
        : m_bbox(bbox) { } //constructor

    //general extraction function, which takes a ray/segment or line
    //object and writes it into the segment list by using CGAL's
    //inbuilt casting function
    template <class RSL>
    void crop_and_extract_segment( const RSL&  rsl )
    {
        CGAL::Object obj = CGAL::intersection(rsl,m_bbox);
        const Segment* s=CGAL::object_cast<Segment>(&obj);
        if (s) m_cropped_vd.push_back(*s);
    }

    //Explicitly providing definitions for rays, lines and segments
    
    void operator<<(const Rep::Ray_2& ray)
    { crop_and_extract_segment(ray); }
    
    void operator<<(const Rep::Line_2& line)
    { crop_and_extract_segment(line); }

    void operator<<(const Segment& seg)
    { crop_and_extract_segment(seg); }

    //Defining << for pairs of segments
    void operator<<(const std::pair<Segment,Segment> & seg_pair)
    {
        m_cropped_vd.push_back(seg_pair.first);
        m_cropped_vd.push_back(seg_pair.second);
    }

    void reset() {
        m_cropped_vd.erase(m_cropped_vd.begin(), m_cropped_vd.end());
    }
};



//hash function wrapper for the parent hash table in SitesStore
struct hash_pair {
    template <class T1, class T2>
    size_t operator()(const std::pair<T1, T2>& p) const
    {
        auto hash1 = std::hash<T1>()(p.first);
        auto hash2 = std::hash<T2>()(p.second);
        return hash1 ^ hash2;
    }
};

//validation function for points
//checks that coordinates are both defined
inline bool   is_infinite( const Point  & pnt )
{
#ifdef EXACT_ART
    return  false;
#else
    real  x = pnt.x();
    real   y = pnt.y();
    return  isnan( x )  ||  isnan( y );
#endif
}

//wrapper function to standard cairo move function
//moves to the coordinates of the Point pnt
void  cairo_move_to( cairo_t  *cr, const Point  & pnt )
{
    cairo_move_to(cr, td( pnt.x() ), td( pnt.y() ) );
}

//wrapper function to standard cairo line function
//draws a line to the coordinates of Point pnt
void  cairo_line_to( cairo_t  *cr, const Point  & pnt )
{
    cairo_line_to(cr, td( pnt.x()), td( pnt.y() ) );
}

/**
 * @param filename - name of the file to write points into
 * @param n - number of points per row of the hexagonal mesh
 *
 * This function creates a hexagonal mesh and writes coordinates to
 * file specified by "filename" There are roughly n points per
 * row/col, mapping into a 2500 x 2500 region The total number of
 * points scales as n^2
 */
void  write_hex_mesh( const char  * filename, int  n )
{
    FILE  * fl;

    assert( n > 1 );
    
    fl = fopen( filename, "wt" );
    assert( fl != NULL );

    //spacing in y needs to be roughly sin(pi/3) x spacing in x
    real  x_spacing = 2500.0 / n;
    real  y_spacing = sqrt(3.0)*td(x_spacing)/2.0;

    //hexagonal points are generated one column at a time
    for  ( int  ind = 0; ind < n; ind++ ) {
        real  y, x;

        //changing y distance every time we go to a new column
        y = (((double)ind) + 1.0) * td(y_spacing);
        
        //to create a hex mesh, even indices are shifted by spacing/2 in x
        x = td(x_spacing) / 2.0;
        if  ( ( ind % 2 ) == 1 )
            x = x_spacing;
            
        //points are written to file one column at a time
        for  ( int  jnd = 0; jnd < n; jnd++ ) {
            fprintf( fl, "%g %g\n", td(x), td(y) );
            
            x += x_spacing;
        }
    }
           
    //closing file
    std::cout<<"Finished generating hex mesh!\n";
    fclose( fl );
}

/**
 * @param filename - name of file into which coordinates should be written
 * @param n - number of random points to be generated
 *
 * This generates "n" random points in the 2500x2500 region and writes
 * them into the file given by "filename"
 */
void  write_random_mesh( const char  * filename, int  n )
{
    FILE  * fl;

    assert( n > 1 );
    
    fl = fopen( filename, "wt" );
    assert( fl != NULL );

    real  len = 2500.0;
    
    for  ( int  ind = 0; ind < n; ind++ ) {
        real  y, x, r_x, r_y;

        //generating random x and y coordinates between 0 and 1
        realRand( r_x );
        realRand( r_y );

        //mapping the random points into len x len region
        x = r_x * len;
        y = r_y * len;

        //output for debugging/status updates
        fprintf( fl, "%g %g\n", td(x), td(y) );
    }
    
    //closing file
    std::cout<<"Finished generating random mesh!\n";
    fclose( fl );
}

/**
 * @param filename - filename to read points from
 * @param ps       - PointSet object into which points will be written
 * @param f_random_sample - a bool to decide whether to sample 5000 points or
 *                   use all points
 *
 * This function reads points in filename and writes them into the
 * pointset object ps The file must contain two coordinates as doubles
 * separated by a space per line The coordinates must be preprocessed
 * to lie in a 2500x2500 square If f_random_sample is set to true, min(5000,
 * given no of points) random distinct points are chosen
 */
void  read_point_set( const char  * filename, PointSet  & ps,
                      bool f_random_sample, int & count, int & actual,
                      int  random_sample_size )
{
    FILE  * fl;

    fl = fopen( filename, "rt" );
    assert( fl != NULL );

    count = 0;
    actual = 0;
    PointSet qs;
    //Reading the file object to extract all points stored in it
    //Points are first stored in intermediate object qs, and finally
    //stored in ps depending on whether we are random sampling or not
    while  ( ! feof( fl ) ) {
        double  x, y;
        int  items;
        
        //Format must be a pair of space separated doubles per line
        items = fscanf( fl, "%lg %lg\n", &x, &y );
        if  ( items != 2 )
            break;
        
        //The function realRand adds a small "noise" factor to perturb points
        real xn, yn;
        realRand( xn );
        realRand( yn );
        
        //printf( "xn: %g\n", td( xn ) );
        //printf( "yn: %g\n", td( yn ) );
        //ps.push_back( Point( x + count/100000.0, y + count /100000.1 ) );
        
        //When sampling, we store points in an intermediate PointSet object qs
        //Otherwise we push straight into ps
        count += 1;
        if ( f_random_sample ) {
            qs.push_back(Point( x + xn, y + yn ) );
        } else {
            ps.push_back( Point( x + xn, y + yn ) );
            actual += 1;
        }
    }
    
    // Adding either all points if fewer than random_sample_size, or
    // upt to limit randomly sampled points
    if  ( f_random_sample ) {
        if ( count < random_sample_size ) {
            ps = qs;
            actual = count;
        } else {
            actual = random_sample_size;
            std::random_shuffle( qs.begin(), qs.end() );
            for  ( int i = 0; i < random_sample_size; ++i )
                ps.push_back(qs[i]);
        }
    }
    
    //closing file
    fclose( fl );
}



//////////////////////////////////////////////////////////////////////////
/// SitesStore - a mechanism to store sites and extra info for them...
//////////////////////////////////////////////////////////////////////////

class SiteExt;

typedef Site  * SitePtr;
typedef SiteExt  * SiteExtPtr;

//This class extends the site datatype for use in our Apollonius graphs
class  SiteExt
{
private:
    Site  site; //Basic Site object
    int  gen; //Represents the front number that a point is on wrt source
    int bfs_gen; //Represents the bfs layer of a site wrt source
    SiteExtPtr bfs_par;
    real   radius; //The weight of the site
    bool  f_start; //A special bool to distinguish the source
    
public:
    //Copy Constructor
    SiteExt( const Site  & _site ) {
        site = _site;
        gen = -1;
        bfs_gen = -1;
        radius = 0;
        f_start = false;
    }

    //Default Constructor
    SiteExt() {
        gen = -1;
        bfs_gen = -1;
        radius = 0;
        f_start = false;
    }

    //Helper function
    void  init( real  x, real  y, real  rad ) {
        site = Site(Traits::Point_2( x, y), Site::Weight(rad));
        gen = -1;
        bfs_gen = -1;
        radius = rad;
    }

    //Getters and Setters
    void  set_gen( int  _g ) { gen = _g; }
    int   get_gen() const { return  gen; }
    
    void set_bfs_gen(int _bg) {bfs_gen = _bg;}
    int get_bfs_gen() const {return bfs_gen;}
    
    void set_bfs_par(SiteExtPtr p) {bfs_par = p;}
    SiteExtPtr get_bfs_par() const{return bfs_par;}
    
    const Site  & loc() const { return  site; }
    SitePtr   loc_ptr() { return  &site; }

    void  set_start_vertex( bool  flag ) { f_start = flag; }
};



//A comparison function for use with std::map on site pointers
struct SitePtrComp {
    bool operator() (const SitePtr  & l,
                     const SitePtr  & r) const {
        if  ( l->x() < r->x() )
            return  true;
        if  ( l->x() > r->x() )
            return  false;
        if  ( l->y() < r->y() )
            return  true;
        if  ( l->y() > r->y() )
            return  false;
        if  ( l->weight() < r->weight() )
            return  true;
      
        return  false;
    }
};

/////////////////////////////////////////////////////////////////////////
// This class performs important bookkeeping on sites It stores all
// site coordinates in the vector "sites" It also builds a dict
// "sites_extra" mapping site ptrs to siteExt ptrs Finally, it stores
// the parent hash table for the path construction
/////////////////////////////////////////////////////////////////////////
class  SitesStore
{
private:
    std::vector<SiteExtPtr>  sites; //Vector of sites

    //Dict mapping *sites to *siteExts
    std::map<SitePtr,SiteExtPtr,SitePtrComp> sites_extra;

    //hash table of parents
    std::unordered_map<std::pair<double,double>, VertexH, hash_pair >
    parents_dict;

public:
    
    //getter function
    SiteExtPtr  get( int  i ) {
        assert( i >= 0 );
        assert( i < (int)sites.size() );
        return  sites[ i ] ;
    }
    
    //store function, adding to both sites and sites_extra
    void  store_site( SiteExtPtr p_site ) {
        assert( p_site != NULL );
        sites.push_back( p_site );
        sites_extra[ p_site->loc_ptr() ] = p_site;
    }
    
    //function which makes "parent" the parent of site "s"
    void store_parent(Site & s, VertexH & parent){
        double x,y;
        x = s.x();
        y = s.y();
        parents_dict[std::make_pair(x,y)] = parent;
    }
    
    //function that looks up the parent of s in the hash table
    VertexH find_parent(Site &s)
    {
        //auto it = parents_dict.find(&s);
        double x,y;
        x = s.x();
        y = s.y();
        auto it = parents_dict.find(std::make_pair(x,y));
        if (it != parents_dict.end())
            {
                return it->second;
            }
        //assert( false );

        return  NULL;
    }

    //getter function for size
    int size() const { return  sites.size(); }
    
    //Storage function given coordinates and weight, making a new site
    SiteExt  * store_site( real  x, real  y, real  rad ) {
        Site st(Traits::Point_2(x,y), Site::Weight(rad) );
        
        SiteExt  * p_site = new SiteExt( st );
        store_site( p_site );
        
        return  p_site;
    }
    
    //Storage function given a site
    SiteExt  * store_site( Site  & s ) {
        return  store_site( s.point().x(), s.point().y(), s.weight() );
    }

    //find function to look up siteExt for a given Site
    SiteExtPtr find( const Site  & s ) {
        /*
          for  ( auto p : sites )  {
          if  ( p->loc() == s )
          return  p;
          }
        */
        auto it = sites_extra.find(SiteExt(s).loc_ptr());
        if (it != sites_extra.end())
            {
                return it->second;
            }

        return  NULL;
    }
    
    ~SitesStore()
    {
        std::for_each(sites.begin(), sites.end(), [](SiteExtPtr p){delete p;});
    }
};



//////////////////////////////////////////////////////////////////////
/// Additive weighted Voronoi diagram functions and classes...
//////////////////////////////////////////////////////////////////////


//Transform class used to draw diagrams
class Transform
{
private:
    Point  src;
    real  v_scale;

public:
    void  set_bottom_left( const Point & p ) { src = p; }
    void  set_scale( real _s ) { v_scale = _s; }

    Point  trans( const Point  & p ) {
        return  Point( v_scale * (p.x() - src.x()),
                       v_scale * (p.y() - src.y() ) );
    }
    real  scale( real  val ) {
        return  v_scale * val;
    }
};


/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param p - pointer to vertex location
 * @param rgb - color variable
 *
 * This function colors the vertex at "p" with the color "rgb"
 */
void  draw_vertex( cairo_t  * cr, Transform  & t, SiteExtPtr  p,
                    Color  & rgb )
{
    cairo_set_line_width(cr, 5.0);

    Point  cen = t.trans( p->loc().point() );
    real   rad = 20.0;

    //setting the color
    rgb.set_source_rgb( cr );
    
    //Creating a filled circle
    cairo_move_to( cr, td(cen.x() + rad), td(cen.y()) );
    cairo_arc(cr, td(cen.x()), td(cen.y()), td(rad), 0, 2 * M_PI);
    cairo_fill(cr);
    cairo_stroke_preserve( cr );
}



/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param src - pointer to source vertex
 * @param trg - pointer to target vertex
 *
 * This function colors the source and target vertices in a light
 * green to differentiate them from the other vertices
 */
void  draw_st( cairo_t  * cr, Transform  & t,
               SiteExtPtr  src, SiteExtPtr  trg )
{
    draw_vertex( cr, t, src, c_src );
    draw_vertex( cr, t, trg, c_trg );
}

/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param src - pointer to source vertex
 * @param trg - pointer to target vertex
 *
 * This function colors the source and target vertices in a light
 * green, and the vertices along the bfs path in a darker green
 */
void draw_st_bfs(cairo_t *cr, Transform &t, SiteExtPtr src, SiteExtPtr trg)
{
    cairo_set_line_width(cr, 5.0);
    //drawing source and vertex
    draw_st(cr,t,src,trg);
    //drawing points along bfs path
    
    SiteExtPtr p = trg->get_bfs_par();
    int len = trg->get_bfs_gen();
    for  ( int  i = 0; i < (len-1); ++i ) {
        Point  cen = t.trans( p->loc().point() );
        real   rad = 10.0;
        
        cairo_set_source_rgb (cr, 1.0, 0.1, 0.0);
        cairo_move_to( cr, td(cen.x() + rad), td(cen.y()) );
        cairo_arc(cr, td(cen.x()), td(cen.y()), td(rad), 0, 2 * M_PI);
        cairo_fill(cr);
        
        cairo_stroke_preserve( cr );
        p = p->get_bfs_par();
    }
}


/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param p - pointer to center of the cell to be colored
 * @param agp - reference to apollonius graph object
 * @param bb - bounding box of the graph
 * @param c_fill - color object for the polygon's interior
 * @param c_frame - color object for the polygon's boundary
 * This function fills the cell centered at "p" with the color "c_fill"
 * The edges are colored with the color "c_frame"
 */
void  draw_cell( cairo_t  * cr, Transform  & t, SiteExtPtr p,
                 WVoronoi  & agp, Box2d  & bb,
                     Color  & c_fill, Color & c_frame )
{
    //Obtaining the vertex handle to the point
    Point p_loc = p->loc().point();
    VertexH w = agp.nearest_neighbor(p_loc);
    
    //Obtaining the edges of the cell
    EdgeCirc ec = agp.incident_edges(w);
    EdgeCirc ec_end = ec;
    
    //Initializing a stream object to hold segments
    Cropped_voronoi_from_apollonius str_obj(bb);

    //printf("///- start ---------------------------------\n" );
    //setting the color for the polygon's interior
    c_fill.set_source_rgb( cr );
    cairo_new_path( cr );
    
    //a variable to move_to the first ever vertex
    bool first = true;
    
    do {
        Segment s;
        
        str_obj.reset();
        
        agp.draw_dual_edge(*ec, str_obj); //inbuilt draw function
        
        //extracting the segment from the stream object
        for ( Segment seg : str_obj.m_cropped_vd) {
            //printf( "seg\n" );
            if  ( is_infinite( seg.source() )  ||  is_infinite( seg.end() ) )
                continue;
            
            if ( first ) {
                Point  src = t.trans( seg.source() );
                
                //printf( " start: (%g, %g)\n", (double)src.x(),
                //        (double)src.y() );
                cairo_move_to(cr, src.x(), src.y() );
                first = false;
            }
            Point  trg = t.trans(seg.target() );
            //printf( " line to: (%g, %g)\n", (double)trg.x(),
            //        (double)trg.y() );
            cairo_line_to( cr, trg );
        }        
    }  while ( ++ec != ec_end );
    //printf( "\\-close--------------------\n\n" );
    
    //closing the polygon and filling its interior
    cairo_close_path(cr);
    cairo_fill_preserve(cr);
    
    //coloring the boundary
    c_frame.set_source_rgb( cr );
    cairo_stroke(cr);
}

/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param agp - reference to apollonius graph object
 * @param bb - bounding box of the graph
 * @param src - pointer to source vertex
 * @param trg - pointer to target cell
 * This function fills the cells along a bfs traversal path from
 * src to trg with the predefined colors c_middle_cells_fill in the interior
 * and c_middle_sites_boundary on the boundary
 */
void draw_bfs_path_cells( cairo_t *cr,
                            Transform &t,
                          WVoronoi  & agp, Box2d  & bb,
                            SiteExtPtr src,
                            SiteExtPtr trg )
{
    //Obtaining the parent to trg, moving backwards to src
    SiteExtPtr p = trg->get_bfs_par();
    
    //obtaining the number of vertices on the path
    int len = trg->get_bfs_gen();

    //filling each cell until we're at src
    for  ( int  i = 0; i < (len-1); ++i ) {
        draw_cell( cr, t, p, agp, bb, c_middle_cells_fill,
                   c_middle_sites_boundary );

        p = p->get_bfs_par();
    }
}


/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param src - source vertex pointer
 * @param trg - target vertex pointer
 * @param ss - SitesStore object of additional points between src and
 *              target to be drawn
 *
 * This calls draw_st to draw source and target in a light green, and
 * then also draws all the points in the ss object in a darker green
 */
void draw_st_ins( cairo_t   * cr,
                   Transform  & t,
                   SiteExtPtr   src,
                   SiteExtPtr   trg,
                   SitesStore & ss,
                   WVoronoi   & ag,
                   Box2d      & bb )
{
    cairo_set_line_width(cr, 5.0);

    //drawing source and vertex
    draw_cell( cr, t, src, ag, bb,
               c_src_fill,
               c_src_boundary );
    draw_cell( cr, t, trg, ag, bb,
               c_trg_fill,
               c_trg_boundary );
    draw_st(cr,t,src,trg);

    //drawing points along the path of insertion
    int len = ss.size();
    for  ( int i = 0; i < len; ++i ) {
        SiteExtPtr p = ss.get(i);
        
        draw_cell( cr, t, p, ag, bb, c_middle_cells_fill,
                   c_middle_sites_boundary );
       
        Point  cen = t.trans( p->loc().point() );
        real   rad = 10.0;

        c_middle_sites_fill.set_source_rgb( cr );
        //cairo_set_source_rgb (cr, 0.0, 0.5, 0.0);
        cairo_move_to( cr, td(cen.x() + rad), td(cen.y()) );
        cairo_arc(cr, td(cen.x()), td(cen.y()), td(rad), 0, 2 * M_PI);
        cairo_fill(cr);
        
        cairo_stroke_preserve( cr );
    }
}


/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param ss - SitesStore object containing the sites of the diagram
 *
 * This function draws all sites of the diagram in the object ss.  The
 * circles are given one of two colors - magenta or yellow - depending
 * on if they are on an odd or even front
 */
void  draw_sites( cairo_t *cr,
                   Transform  & t,
                   SitesStore  & ss
                  )
{
    int sz;

    cairo_set_line_width(cr, 5.0);

    sz = ss.size();

    //Draws sites of sufficiently large weight - the large circles
    for  ( int  i = 0; i < sz; i++ ) {
        SiteExtPtr  p = ss.get( i );

        //printf( " loc: (%g, %g)\n", td(p->loc().point().x()),
        //        td(p->loc().point().y()) );

        real s_w = p->loc().weight();
        if  ( s_w <= epsilon )
            continue;
        Point  cen = t.trans( p->loc().point() );
        real  rad = t.scale( s_w );
        /*printf( "i :%d  cen: (%g, %g)  w: %g rad: %g\n", i,
          td( cen.x()), td( cen.y() ),
          td( s_w ), td( rad ) );*/

        if  ( ( p->get_gen() % 2 ) == 0 )
            cairo_set_source_rgb (cr, 1.0, 1.0, 0.0);
        else
            cairo_set_source_rgb (cr, 1.0, 0.5, 1.0);
        cairo_move_to( cr, td(cen.x() + rad), td( cen.y() ) );
        cairo_arc(cr, td( cen.x() ), td( cen.y() ), td( rad ), 0, 2 * M_PI);
        //cairo_stroke_preserve(cr);
        cairo_fill(cr);
        
        //cairo_set_source_rgb (cr, 1.0, 0.0, 0.0);
        //cairo_arc(cr, cen.x(), cen.y(), rad, 0, 2 * M_PI);
        cairo_stroke( cr );
    }

    // Draw the boundaries of the large circles
    cairo_set_line_width(cr, 5.0);
    for  ( int  i = 0; i < sz; i++ ) {
        SiteExtPtr  p = ss.get( i );

        real s_w = p->loc().weight();
        if  ( s_w <= epsilon )
            continue;
        Point  cen = t.trans( p->loc().point() );
        double  rad = td( t.scale( s_w ) );
        
        cairo_move_to( cr, td( cen.x() + rad), td( cen.y() ) );
        cairo_arc(cr, td( cen.x()), td( cen.y() ),
                  td( rad ), 0, 2 * M_PI);
        cairo_set_source_rgb (cr, 1.0, 0.0, 0.0);
        cairo_stroke( cr );
    }

    /// Fill in the point size sites (i.e., input points)
    for  ( int  i = 0; i < sz; i++ ) {
        SiteExtPtr  p = ss.get( i );

        double s_w = td( p->loc().weight() );

        //printf( "sw: %g  > %g\n", td(s_w), td(epsilon) );
        if  ( s_w > epsilon )
            continue;
        //printf( "POINT out!\n" );
        Point  cen = t.trans( p->loc().point() );
        double  rad = 5.0;
        
        cairo_set_source_rgb (cr, 1.0, 0.0, 1.0);
        cairo_move_to( cr, td(cen.x() + rad), td( cen.y() ) );
        cairo_arc(cr, td( cen.x() ), td( cen.y() ), td( rad ), 0, 2 * M_PI);
        cairo_fill(cr);

        cairo_stroke_preserve( cr );
    }
    //printf( "Draw sites done\n\n" );
}

/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param bb - bounding box of points
 * @param ag - apollonius graph
 *
 * This function uses the cairo library to draw the edges of the dual
 * of the graph
 */
void  draw_edges( cairo_t *cr,
                  Transform  & t,
                  Box2d  & bb,
                  WVoronoi  & ag
                  )
{
    //printf( "Drawing edges...\n" );
    Cropped_voronoi_from_apollonius crop( bb );
    //printf( "after cropping...\n" );
                
    cairo_set_source_rgb (cr, 0.0, 0.0, 0.0);
    cairo_set_line_width (cr, 5.0 );
    cairo_set_line_cap  (cr, CAIRO_LINE_CAP_ROUND); /* Round dot*/

    //printf( "Drawing dual...\n" );
    ag.draw_dual( crop );
    //printf( "after Drawing dual...\n" );

    for ( Segment seg : crop.m_cropped_vd) {
        //printf( "seg\n" );
        if  ( is_infinite( seg.source() )  ||  is_infinite( seg.end() ) )
            continue;
        
        cairo_move_to(cr, t.trans( seg.source() ) );
        cairo_line_to(cr, t.trans( seg.end() ) );
        cairo_stroke(cr);
    }
}

/**
 * @param ag - apollonius graph object to be drawn
 * @param ss - SitesStore object containing sites of ag
 * @param bb - bounding box of points
 * @param filename - name of the file into which diagram should be written
 *
 * This function draws the dual of the apollonius graph without
 * specially marked vertices
 */
void  draw_diagram( WVoronoi  & ag,
                    SitesStore  & ss,
                    Box2d  & bb, const char  * filename )
{
    cairo_t *cr;
    cairo_surface_t *surface;
    Transform  t;

    double  scale = 1.0;
    
    t.set_bottom_left( Point( bb.xmin(), bb.ymin() ) );
    t.set_scale( scale );

    //printf( "Bottom left: (%g, %g)\n", bb.xmin(), bb.ymin() );

    int  w = scale * ( bb.xmax() - bb.xmin() );
    int  h = scale * ( bb.ymax() - bb.ymin() );

    //printf( "Width, height: %d, %d\n", w, h );
    
    surface = (cairo_surface_t *)cairo_pdf_surface_create( filename, 2 + w, 2 + h );
    //surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, 2 + w, 2 + h );
    cr = cairo_create(surface);

    // Draw background
    cairo_rectangle(cr, 0, 0, 2+w, 2+h );
    cairo_set_source_rgb(cr, 0.8, 0.8, 1.0);
    cairo_fill(cr);
    
    //draw_sites( cr, t, ss );
    draw_edges( cr, t, bb, ag );
    //printf( "draw_edges_done\n" );

    //draw_input_sites( cr, t, ss );
    draw_sites(cr,t,ss);
    //printf( "draw_sites done\n" );

    cairo_show_page (cr);
    //cairo_surface_write_to_png(surface, filename);
    cairo_surface_flush(surface);
    printf("Diagram complete\n");
    cairo_surface_destroy (surface);
    cairo_destroy (cr);
}

/**
 * @param cr - pointer to cairo object
 * @param t - transform object
 * @param bb - bounding box of the graph
 * @param ag - reference to apollonius graph object
 * @param src - pointer to source vertex
 * @param trg - pointer to target cell
 * This function fills the cells of src and trg with predefined colors defined above
 */
void  draw_st_cells( cairo_t  * cr, Transform  & t,
                      Box2d  & bb,
                      WVoronoi   & ag,
                     SiteExtPtr  src, SiteExtPtr  trg )
{
    //filling the src cell
    draw_cell( cr, t, src, ag, bb,
               c_src_fill,
               c_src_boundary );
    
    //filling the trg cell
    draw_cell( cr, t, trg, ag, bb,
               c_trg_fill,
               c_trg_boundary );
}

/**
 * @param ag - apollonius graph object to be drawn
 * @param ss - SitesStore object containing sites of ag
 * @param src - source vertex pointer
 * @param trg - target vertex pointer
 * @param bb - bounding box of points
 * @param filename - name of the file into which diagram should be written
 *
 * This function draws the initial unweighted voronoi diagram with src
 * and target drawn green and input points drawn red The colors (and
 * other properties) can be modified by changing draw_st and
 * draw_sites respectively
 */
void draw_initial_diagram( WVoronoi  & ag,
                           SitesStore  & ss,
                           SiteExtPtr src, SiteExtPtr trg,
                           Box2d  & bb, const char  * filename )
{
    cairo_t *cr;
    cairo_surface_t *surface;
    Transform  t;

    double  scale = 1.0;

    t.set_bottom_left( Point( bb.xmin(), bb.ymin() ) );
    t.set_scale( scale );

    //printf( "Bottom left: (%g, %g)\n", bb.xmin(), bb.ymin() );

    int  w = scale * ( bb.xmax() - bb.xmin() );
    int  h = scale * ( bb.ymax() - bb.ymin() );

    //printf( "Width, height: %d, %d\n", w, h );
    
    surface = (cairo_surface_t *)cairo_pdf_surface_create( filename, 2 + w, 2 + h );
    //surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, 2 + w, 2 + h );
    cr = cairo_create(surface);

    // Draw background
    cairo_rectangle(cr, 0, 0, 2+w, 2+h );
    cairo_set_source_rgb(cr, 0.8, 0.8, 1.0);
    cairo_fill(cr);
    
    //draw_sites( cr, t, ss );
    draw_st_cells( cr, t, bb, ag, src, trg );

    draw_edges( cr, t, bb, ag );
    //printf( "draw_edges_done\n" );


    
    //SiteExtPtr  src,
    // SiteExtPtr  trg                   

    draw_sites( cr, t, ss );
    //printf( "draw_sites done\n" );

        
    draw_st(cr,t,src,trg);

        

    cairo_show_page (cr);
    //cairo_surface_write_to_png(surface, filename);
    cairo_surface_flush(surface);
    printf("Diagram complete\n");
    cairo_surface_destroy (surface);
    cairo_destroy (cr);
}

/**
 * @param ag - Apollonius graph object to be drawn with inserts along bfs
 * @param ss - SitesStore object of all the sites of the graph
 * @param src - pointer to source site
 * @param trg - pointer to target site
 * @param bb - bounding box for diagram
 * @param filename - name of file to which to draw diagram into
 *
 * This function marks the sites along the bfs traversal from src to
 * trg for visualization
 *
 * generates *_bfs.pdf
 */
void draw_diagram_bfs_inserts( WVoronoi  & ag,
                               SitesStore  & ss,
                               SiteExtPtr src, SiteExtPtr trg,
                               Box2d  & bb, const char  * filename )
{
    cairo_t *cr;
    cairo_surface_t *surface;
    Transform  t;

    double  scale = 1.0;
    
    t.set_bottom_left( Point( bb.xmin(), bb.ymin() ) );
    t.set_scale( scale );

    //printf( "Bottom left: (%g, %g)\n", bb.xmin(), bb.ymin() );

    int  w = scale * ( bb.xmax() - bb.xmin() );
    int  h = scale * ( bb.ymax() - bb.ymin() );

    //printf( "Width, height: %d, %d\n", w, h );
    
    surface = (cairo_surface_t *)cairo_pdf_surface_create
        ( filename, 2 + w, 2 + h );

    //surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, 2 + w, 2 + h );
    cr = cairo_create(surface);

    // Draw background
    cairo_rectangle(cr, 0, 0, 2+w, 2+h );
    cairo_set_source_rgb(cr, 0.8, 0.8, 1.0);
    cairo_fill(cr);

    draw_bfs_path_cells( cr, t, ag, bb, src, trg );
    draw_st_cells( cr, t, bb, ag, src, trg );


    
    //draw_sites( cr, t, ss );
    draw_edges( cr, t, bb, ag );
    //printf( "draw_edges_done\n" );
    draw_sites( cr, t, ss );
    //printf( "draw_sites done\n" );
    
    draw_st_bfs(cr,t,src,trg);

    cairo_show_page (cr);
    cairo_surface_flush(surface);
    //printf("Diagram complete\n");
    cairo_surface_destroy (surface);
    cairo_destroy (cr);
}


/**
 * @param ag       - apollonius graph object to be drawn
 * @param ss       - SitesStore object containing sites of ag
 * @param ss_ins   - SitesStore object containing only the inserted
 *                   points computed by the algorithm
 * @param src      - source vertex pointer
 * @param trg      - target vertex pointer
 * @param bb       - bounding box of points
 * @param filename - name of the file into which diagram should be written
 *
 * This function calls suitable functions to draw a pdf file of the
 * (unweighted) voronoi diagram with inserted points Input points are
 * red and inserted points are green, this behavior can be changed by
 * modifying draw_st_ins
 *
 * Output: *_alg.pdf
 */
void draw_diagram_inserts( WVoronoi  & ag,
                            SitesStore  & ss,
                            SitesStore & ss_ins,
                            SiteExtPtr src, SiteExtPtr trg,
                            Box2d  & bb, const char  * filename )
{ 
    cairo_t *cr;
    cairo_surface_t *surface;
    Transform  t;

    double  scale = 1.0;
    
    t.set_bottom_left( Point( bb.xmin(), bb.ymin() ) );
    t.set_scale( scale );

    //printf( "Bottom left: (%g, %g)\n", bb.xmin(), bb.ymin() );

    int  w = scale * ( bb.xmax() - bb.xmin() );
    int  h = scale * ( bb.ymax() - bb.ymin() );

    //printf( "Width, height: %d, %d\n", w, h );
    
    surface = (cairo_surface_t *)cairo_pdf_surface_create
        ( filename, 2 + w, 2 + h );

    //surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, 2 +
    //w, 2 + h );
    cr = cairo_create(surface);

    // Draw background
    cairo_rectangle(cr, 0, 0, 2+w, 2+h );
    cairo_set_source_rgb(cr, 0.8, 0.8, 1.0);
    cairo_fill(cr);
    
    
    //draw_sites( cr, t, ss );
    draw_sites( cr, t, ss );
    draw_st_ins(cr,t,src,trg,ss_ins, ag, bb);
    draw_edges( cr, t, bb, ag );
    //printf( "draw_edges_done\n" );

    //printf( "draw_sites done\n" );
    cairo_show_page (cr);
    //draw_edges( cr, t, bb, ag );
    //cairo_surface_write_to_png(surface, filename);
    cairo_surface_flush(surface);
    printf("Diagram complete!\n");
    cairo_surface_destroy (surface);
    cairo_destroy (cr);
}
//////////////////////////////////////////////////////////////////////////
/// The algorithm
//////////////////////////////////////////////////////////////////////////


class  Algorithm
{
private:
    PointSet  ps; // point set of input points, read from a file
    WVoronoi  ag; // internal apollonius graph to execute algorithm on
    SitesStore  sites; // augmented site storage object
    Box2d  bb_ps; // bounding box of the point set for drawing purposes
    Box2d  bb_draw; // bounding box for drawing purposes
    SiteExtPtr start; //starting vertex
    SiteExtPtr end; //target vertex
    int  curr_gen; //variable storing the front at which we are
    //bool  f_refine_bisectors;
    int count, actual;
    bool done; //for termination condition
    bool f_random_sample; //to control whether to use all points or not
    int   random_sample_size;
    
    std::queue<VertexH> q_curr; //queue for candidate insertion points
    std::queue<VertexH> bfs_q; //queue for bfs search
    
    //private member functions, described in comments above their
    //implementations
    bool  is_create_vertex( FaceCirc fc, VertexH v);
    void  get_sites_on_vor_vertices( VertexH  v,
                                     SitesVector  & new_sites_queue );

    //void  get_sites_on_bisectors( VertexH  v,CropPointCollector & cpc );
    void reconstruct(FaceCirc fc, VertexH v);
    void getBFSdist();

    std::string output_init_name, output_bfs_name, output_inserts_name,
        output_disks_name, output_stats_name, base_name;

public:
    Algorithm() {
        start = NULL;
        done = false;
        f_random_sample = false;
        random_sample_size = -1;
    }

    void  set_output_name_base( const char  * _base ) {
        std::string  base = _base;

        base_name = _base;
        output_init_name = base + "_initial.pdf";
        output_bfs_name = base + "_bfs.pdf";
        output_inserts_name = base + "_alg.pdf";
        output_disks_name = base + "_disks.pdf";
        output_stats_name = base + "_stats.txt";
    }
    
    void  read( const char  * filename ) {
        read_point_set( filename, ps, f_random_sample, count, actual,
                        random_sample_size );
        printf("Successfully read point set!\n");
    }
    
    //function to insert input point set
    void   insert_point_set( PointSet  & pset, int  gen );
    void   setRandom( bool  b ) { f_random_sample = b; }

    //void  insert_point_set();
    //function to vertices at boundary of a cell v
    void   compute_new_sites_around( VertexH  v,
                                    SitesVector  & new_sites_queue);
    //function to handle individual fronts and set up queues for the next front
    void   handle_front();
    //overarching function that runs the algorithm
    void   run();
    void   write_stats();

};


void   Algorithm::write_stats()
{
    FILE  * fl;
    
    fl = fopen( output_stats_name.c_str(), "wt" );
    assert(fl!=NULL);
    if(count!=actual)
        fprintf( fl, "%% The input set was sampled down from input size \n"
                 "%% of %d to an output size of %d for visualization\n", \
                 count, actual);
    else
        fprintf(fl, "%% The size of input point set was %d \n",
                count);
    fprintf( fl, "%% The length of the bfs path was %d \n",
             end->get_bfs_gen() - 1);
    fprintf( fl, "%% The length of the algorithm's path was %d\n",
             curr_gen - 2);
    fprintf( fl, "%20s   &   %'6d  &   %'6d &  %'6d \\\\\n",
             base_name.c_str(), 
             count,
             end->get_bfs_gen() - 1,
             curr_gen - 2 );
    
    fclose( fl );
}



///////////////////////////////////////////////////////////////////////////////
// Helper functions for site comparison and distance
///////////////////////////////////////////////////////////////////////////////

bool lex_sites_comp( const Site   & a, const Site  & b )
{
    if  ( a.x() < b.x() )
        return  true;
    if  ( a.x() > b.x() )
        return  false;
    if  ( a.y() < b.y() )
        return  true;
    if  ( a.y() > b.y() )
        return  false;
    if  ( a.weight() < b.weight() )
        return  true;
    if  ( a.weight() > b.weight() )
        return  false;
    return  false;
}


real  dist( const Site  & a, const Site  & b )
{
    real   dx = a.x() - b.x();
    real   dy = a.y() - b.y();

    return  sqrt( td(dx*dx + dy*dy) ) - a.weight() - b.weight();
}

real   dist( const Site  & a, const Point  & b )
{
    real  dx = a.x() - b.x();
    real  dy = a.y() - b.y();

    return  sqrt( td( dx*dx + dy*dy ) ) - a.weight();
}


/////////////////////////////////////////////////////////////////////////////
// Implementations of the Algorithm Class's member functions
/////////////////////////////////////////////////////////////////////////////
/**
 * @param fc - face circulator around penultimate vertex to target vertex
 * @param v  - vertex handle of penultimate vertex to target vertex
 *
 * This function finds the shortest path from start to end using the
 * parent dictionary maintained in the SitesStore class
 */
void Algorithm::reconstruct(FaceCirc fc, VertexH v)
{
    printf( "Target point reached!\n");
    printf( "The triangulation only distance was "
            "%d, optimal distance was %d\n",
            end->get_bfs_gen() - 1, curr_gen - 2);
    printf("writing stats to txt file...\n");
    write_stats();

    printf("Drawing the additive diagram with disks ...\n");
    draw_diagram( ag, sites, bb_draw, output_disks_name.c_str());
    printf( "Drawing the end results...\n" );

    //Build a second voronoi diagram of only input points
    WVoronoi ag2;
    SitesStore ss2, ss2_ins, ss_ins_bfs;
    for ( Site s : ps ) {
        ag2.insert(s);
        ss2.store_site(s);
    }
    
    printf("Drawing initial diagram...\n");
    draw_initial_diagram( ag2, ss2, start, end, bb_draw,
                          output_init_name.c_str() );
    
    printf("Drawing diagram along bfs path...\n");
    draw_diagram_bfs_inserts( ag2, ss2, start, end, bb_draw,
                              output_bfs_name.c_str());
    
    //Using fc and v to start the path construction,
    //with the parent of end being the site of v
    //add a point "at" at the site between v and end
    Site at;
    ag.dual( fc ).assign<Site>( at);
    SiteExtPtr pval = sites.find(v->site());
    Site parent = pval->loc();
    int gen = pval->get_gen();
    
    
    double  atx, aty, parx, pary;
    // double sx = start->loc().x(); //x coordinate of the starting point
    // double sy = start->loc().y(); //y coordinate of the starting point
    atx = at.x(); //the current x coordinate of the inserted point
    aty = at.y(); //the current y coordinate of the inserted point
    int i = 0;

    do {
        parx = parent.x(); // x coordinate of the parent
        pary = parent.y(); //y coordinate of the parent
        double dist = pow( pow(atx - parx,2.0) + pow(aty - pary, 2.0) , 0.5);
        //std::cout<<"parent weight is " << parent.weight();
        double ratio = parent.weight()/dist;
        //std::cout << "distance is "<< dist;
        if(ratio == 0 || dist == 0)
            break;
        Traits::Point_2 coord ( (1.0 - ratio)*parx + ratio*atx ,
                                (1.0-ratio)*pary + ratio*aty);
        //computing the intersection of the circle and line joining
        //the sites

        //creating a site at the intersection with 0 weight
        Traits::Site_2 bridgepoint(coord, Traits::Site_2::Weight(0));
        
        //adding intermediate "bridge" site to diagram
        ag2.insert(bridgepoint);
        ss2.store_site(bridgepoint);
        ss2_ins.store_site(bridgepoint);
        atx = parx;
        aty = pary;
        Site par = sites.find(parent)->loc();
        parent = sites.find_parent(par)->site();
        i++;

        //terminating when we have one point per front
    }  while  ( i < gen - 1 );

    //drawing the diagram with the bridge points inserted in
    printf("Drawing diagram along optimal path...\n");
    draw_diagram_inserts(ag2, ss2, ss2_ins, start, end,
                         bb_draw, output_inserts_name.c_str());
}



/**
 * @param fc - face circulator around the vertex corresponding to v
 *
 * @param v - vertex handle around which we want sites
 *
 * This function validates whether or not we should compute points
 * around v at all.  It checks the vertices of the face corresponding
 * to "fc" in the apollonius graph.  If any vertices of the face are
 * bounding points, we have hit the edge of the diagram and do not add
 * any points around v If any of the vertices is the end vertex, then
 * we pass control to the function "reconstruct" to construct the
 * shortest path and stop the algorithm Otherwise if any vertex is an
 * as-yet-undiscovered input point, we always return true If no vertex
 * is newly discovered, sometimes a vertex may be part of a front that
 * is too far back and should not be added again. This is ensured by
 * checking that the gen field of all points is at least curr_gen - 1
 */
bool  Algorithm::is_create_vertex( FaceCirc fc, VertexH v)
{
    VertexH  v_a, v_b, v_c;

    //f_special = false;
    
    //Determing vertices and corresponding sites in the graph
    
    v_a = fc->vertex( 0 );
    v_b = fc->vertex( 1 );
    v_c = fc->vertex( 2 );

    SiteExtPtr p_a, p_b, p_c;

    p_a = sites.find( v_a->site() );
    p_b = sites.find( v_b->site() );
    p_c = sites.find( v_c->site() );

    //Checking that sites so found are valid
    assert( p_a != NULL );
    assert( p_b != NULL );
    assert( p_c != NULL );
    
    //Skipping v if any point is on the boundary
    if  ( ( p_a->get_gen() == GEN_BOUNDING )
          ||  ( p_b->get_gen() == GEN_BOUNDING )
          ||  ( p_c->get_gen() == GEN_BOUNDING ) )
        return  false;
    
    //Ending the algorithm if any point is the target
    if  ( (p_a->get_gen() == GEN_TARGET)
         || ( p_b->get_gen() == GEN_TARGET )
         || ( p_c->get_gen() == GEN_TARGET ))
    {
        done = true;
        reconstruct(fc, v);
        return  true;
    }
    

    //Adding vertices around v if any point is the input
    if  ( p_a->get_gen() == GEN_INPUT )
        return  true;
    if  ( p_b->get_gen() == GEN_INPUT )
        return  true;
    if  ( p_c->get_gen() == GEN_INPUT )
        return  true;
    
    //Skipping v if any point is in a front that is too far behind
    if  ( p_a->get_gen() < curr_gen - 1 )
        return  false;
    if  ( p_b->get_gen() < curr_gen - 1 )
        return  false;
    if  ( p_c->get_gen() < curr_gen - 1 )
        return  false;
    
    return false;
}

/**
 * @param v - the vertex handle around which we're finding new site
 * locations
 *
 * @param new_sites_queue - the queue into which sites around v are to
 * be added
 *
 * This function adds all valid points around v into new_sites_queue
 * (validation is handled by is_create_vertex) The points are given
 * slightly dilated radii (by a factor of 1.00001) to avoid floating
 * point degeneracy issues (noticeable speedup occurs in the hexagonal
 * grid cases with this approach) Since the apollonius graph is the
 * dual of the diagram, we iterate over incident faces and assign the
 * dual of those points as new sites.  "Parent" vertices are stored
 * for the path reconstruction at this stage as well, with v being the
 * parent in the path for each added site
 */
void  Algorithm::get_sites_on_vor_vertices( VertexH  v,
                                            SitesVector  & new_sites_queue
                                            )
{
    FaceCirc fc, fc_end;
    fc = ag.incident_faces(v);
    fc_end = fc;

    do {
        if  ( ag.is_infinite( fc ) )
            continue;
        Site new_site;

        //bool  f_special = false;
        
        //validate vertex
        if  ( ! is_create_vertex( fc, v) )
            continue;
        if (done)
            break;
        //add dual of the face as a new site
        ag.dual( fc ).assign<Site>( new_site );
        real rad = dist( v->site(), new_site.point() );
        if  ( rad <= epsilon )
            continue;

        //add a small factor to avoid degeneracy issues
        Site  p(new_site.point(), rad * 1.00001 );
        //Site  p(new_site.point(), rad);
        new_sites_queue.push_back( p );
        
        //store v as parent in path to newly added point p
        sites.store_parent(p, v);
    } while ( ++fc != fc_end );
}

/**
 * @param v - vertex handle for the cell whose vertices we want to
 * find @param new_sites_queue - the queue into which the vertices
 * around v will be stored This function passes control to
 * get_sites_on_vor_vertices after checking that the vertex handle and
 * incident faces around v are valid
 */
void  Algorithm::compute_new_sites_around( VertexH  v,
                                           SitesVector  & new_sites_queue
                                           )
{
    //Checking that the vertex handle is valid
    if  ( v == 0 )
        return;
    if  ( ag.is_infinite( v ) )
        return;

    //Checking that the face circulator is valid
    FaceCirc fc;
    try{
        fc = ag.incident_faces(v);
    }
    catch(...)
        {
            printf("We have probably encountered a hidden site in the additive diagram.\n");
            printf("Continuing the algorithm ...\n");
            return;
        }
    if  ( fc == 0 )
        return;


    //printf( "before: get_sites_on_vor_vertices\n" );
    
    // Go around the adjacent faces, insert every vertex around
    get_sites_on_vor_vertices( v, new_sites_queue );
    
    //printf( "...after get_sites_on_vor_vertices\n" );
}

/**
 * @param sites - a vector from which duplicates are to be removed
 * This function removes any duplicates in a given vector of sites by
 * the standard sorting approach
 */
void  remove_duplicate_sites( std::vector<Site>  & sites )
{
    std::vector<Site>  newer;

    if  ( sites.size() <= 1 )
        return;
    
    sort( sites.begin(), sites.end(), lex_sites_comp );
    for  ( unsigned int  i = 0; i < sites.size(); i++ ) {
        if  ( i == 0 ) {
            newer.push_back( sites[ 0 ] );
            continue;
        }
        if  ( ! ( newer.back() == sites[ i ] ) )
            newer.push_back( sites[ i ] );
    }
    sites = newer;
}


/**
 * This function corresponds to one iteration of the "main loop" of
 * the algorithm in handle_front First, the points of the previous
 * hyperbolic wavefront in q_curr are found by calling the function
 * compute_new_sites_around Then these points (stored in
 * new_sites_queue) are added into both the apollonius graph ag and
 * the site storage object sites.  Vertex handles of the newly added
 * points are stored in q_curr for the next iteration.
 */
void  Algorithm::handle_front()
{
    std::vector<Site>  new_sites_queue;
    
    // Adding all valid points of the vertices of the previous
    // hyperbolic front into new_sites_queue
    while  ( ! q_curr.empty() ) {
        VertexH  v = q_curr.front();
        q_curr.pop();
        
        if  ( v == 0 )
            break;

        //printf( "Before compute_new_sites_around\n" );
        compute_new_sites_around( v, new_sites_queue);
        if(done)
            break;
        //printf( "After compute_new_sites_around\n" );
    }
    
    //found the target point
    if(done)
        return;
    
    //printf( "Computed new sites...\n" );
    // We reached the end of the game...
    if  ( new_sites_queue.size() == 0 )
        return;

    // Remove duplicate sites
    remove_duplicate_sites( new_sites_queue );
    
    // Insert new sites into diagram & add site centers to q_curr for
    // the next iteration
    for  ( auto s : new_sites_queue ) {
        if  ( ! BBox_is_in( bb_ps, s.point() ) )
            continue;
        SiteExtPtr  info = sites.store_site( s );
        info->set_gen( curr_gen );
        
        VertexH  v_n = ag.insert( info->loc() );
        
        q_curr.push( v_n );
    }
}

/**
 * Runs standard BFS on the input Voronoi diagram, starting at vertex
 * "start" until vertex "end" is reached The shortest distance between
 * the terminal vertices is outputted in a printf statement.
 */
void Algorithm::getBFSdist()
{
    VertexH v;
    SiteExtPtr p_ptr;
    printf("\nReached BFS\n");
    int gen = 0;
    while  ( ! bfs_q.empty() ) {
        //bfs_q contains vertex handles, which are converted to
        //siteExtPtrs by looking them up in the sites object
        VertexCirc vc, vc_end;
        v = bfs_q.front();
        p_ptr = sites.find(v->site());
        bfs_q.pop();
        if(p_ptr == NULL)
            continue;
        
        //storing loc and gen fields of the site corresponding to the
        //vertex handle
        Site q = p_ptr->loc();
        gen = p_ptr->get_bfs_gen();
        
        //Checking if we have reached the end
        if(q.x() == end->loc().x() && q.y() == end->loc().y())
            break;
        
        //adding all neighboring points of v, using the bfs_gen field
        //to check if the vertex is visited or unvisited and to store
        //distance
        
        vc = ag.incident_vertices(v);
        vc_end = vc;
        do {
            VertexH to_add = vc.base();
            SiteExtPtr vptr = sites.find(vc->site());
            
            //checking if the site we're looking for is valid
            if(vptr==NULL)
                continue;
            //checking that the site is not on the boundary
            if(vptr->get_gen() == GEN_BOUNDING)
                continue;
            
            //if the vertex is undiscovered, bfs_gen == -1
            
            if(vptr->get_bfs_gen() == -1) {
                //updating gen and par fields for the newly discovered vertex
                vptr->set_bfs_gen(gen + 1);
                vptr->set_bfs_par(p_ptr);
                bfs_q.push(to_add);
            }
        } while ( ++vc != vc_end );
    }
    printf( "The triangulation only distance between start and end was %d\n",
            end->get_bfs_gen() - 1);
}

/**
 * This function carries out the algorithm on the point set First, two
 * additional point sets ps_extra and inners are computed:
 *
 * 1. ps_extra - is a set containing bounding points at the edges of
 *    the input set so that the algorithm does not encounter unbounded
 *    faces. The bounding points are computed by
 *    point_set_add_voronoi_bounding_points
 *
 * 2. inners - contains all points in a rectangular region determined
 *    by params passed to the function
 *    point_set_compute_inner_points. The lower left corner of this
 *    rectangle is chosen as "start", and the point furthest from
 *    start in the rectangle is chosen to be "end" Then, all points in
 *    ps and ps_extra are added into the apollonius graph by
 *    insert_point_set.  start and end vertices are computed as
 *    described above, and are initialized to have appropriate gen and
 *    bfs_gen fields The function getBFSdist is run to give us the
 *    distance between the points along the triangulation, and then
 *    the main loop of the algorithm follows.  The main loop of the
 *    algorithm repeatedly calls handle_front until either the ending
 *    vertex is encountered or all input points have been discovered
 *    by the algorithm
 */
void  Algorithm::run()
{
    PointSet  ps_extra, inners;
    
    //computing the rectangle for starts and ends
    point_set_compute_inner_points(ps, inners, 0.15, 0.8, 0.15, 0.8);
    //inners = ps;
    
    //printf( "ps.size(): %d\n", (int)ps.size() );
    
    //computing bounding points
    bb_ps = ComputeBBox( ps );
    bb_draw = BBox_expand( bb_ps, 1.1 );
    Box2d bb_draw_out = BBox_expand( bb_ps, 1.2 );
    point_set_add_voronoi_bounding_points( ps_extra, bb_draw );
    point_set_add_voronoi_bounding_points( ps_extra, bb_draw_out );
    
    printf("Creating a Voronoi diagram ... \n");
    //adding all points into the apollonius graph
    //printf( "a_ps.size(): %d\n", (int)ps.size() );
    insert_point_set( ps, GEN_INPUT );
    ///printf( "b_ps.size(): %d\n", (int)ps.size() );
    insert_point_set( ps_extra, GEN_BOUNDING );
    //printf( "c_ps.size(): %d\n", (int)ps.size() );
    printf("Voronoi diagram created!\n");
    
    //Picking a starting vertex and initializing gen and bfs_gen correctly
    Point cen = point_set_compute_llmost( inners );
    VertexH  v = ag.nearest_neighbor( cen );
    //printf( "d_ps.size(): %d\n", (int)ps.size() );
    start = NULL;
    start = sites.find( v->site() );
    assert( start != NULL );
    start->set_start_vertex( true );
    start->set_gen( 1 );
    start->set_bfs_gen(0);
    
    
    //Picking and end vertex as the furthest point from start in inners
    Point  furthest = point_set_compute_furthest( inners, cen );
    VertexH  w = ag.nearest_neighbor( furthest );

    end = NULL;
    end = sites.find(w->site());
    assert(end!=NULL);
    end->set_gen(GEN_TARGET);
    
    printf( "cen: (%g, %g)\n", (double)cen.x(), (double)cen.y() );
    printf("furthest is at %Lf, %Lf\n",w->site().x(),w->site().y());
    printf("Start is at %Lf, %Lf\n",v->site().x(),v->site().y());
    
    //    Box2d bb_draw_2 = BBox_expand( bb_ps, 4.1 );
    //Drawing initial output with bounding points
    //draw_diagram( ag, sites, bb_draw_2, "output/01.pdf" );
    
    //performing bfs to compute distance along triangulation
    bfs_q.push(v);
    getBFSdist();
    
    //time variables to time performance
    q_curr.push( v );
    curr_gen = 2;
    
    //main loop
    printf("Running the main algorithm from start until end is reached ...\n");
    while  ( ! q_curr.empty() ) {
        //printf( "Before handle front: %d\n", curr_gen );
        handle_front();
        if(done)
            break;
        //printf( "After handle front: %d\n", curr_gen );
        
        //The following lines may be uncommented to generated the
        //additive graphs for viewing after every front, but this
        //slows run time quite a bit
        //
        //sprintf( buff, "output/%02d.pdf", curr_gen );
        //sprintf( buff, "%02d.pdf", curr_gen );
        //draw_diagram( ag, sites, bb_draw_2, buff );
        //printf("We're at gen %d", curr_gen);
        curr_gen++;
    }
    
    printf( "Exiting normally...\n" );
}

/**
 * @param pset - the input point set
 * @param gen - default gen variable to assign to all points
 * This function adds all points in pset into the apollonius graph ag and sets their gen field to the parameter gen
 * The insertion assumes we are inserting into an unweighted Voronoi diagram
 */
void  Algorithm::insert_point_set( PointSet  & pset, int  gen )
{
    for  ( auto loc : pset ) {
        Site   p(loc, Site::Weight( 0 ));

        SiteExtPtr  info = sites.store_site( loc.x(), loc.y(), 0 );
        info->set_gen( gen );
        
        ag.insert( info->loc() );
    }
}

//////////////////////////////////////////////////////////////////////////
/// Usage and main function
//////////////////////////////////////////////////////////////////////////
/**
 * Prints correct usage if the format received was invalid
 */
void  usage()
{
    printf( "\n\n" );
    printf( "  vor_skip  write_hex_mesh  [hex-mesh-file] [n]\n" );
    printf( "  vor_skip  write_random    [mesh-file]     [n]\n" );
    printf( "  vor_skip  [input-file]    [output_base_name]\n" );
    printf( "\n\n" );
    exit( -1 );
}



void  sparsify( const char  * fn_in, const char  * fn_out )
{
    FILE  * fin, * fout;
    const char  * pos;
    
    fin = fopen( fn_in, "rt" );
    assert( fin != NULL );

    fout = fopen( fn_out, "wt" );
    assert( fout != NULL );

    char  buffer[ 1024 ];
    while  ( !feof( fin ) ) {
        pos = fgets( buffer, 1024, fin );
        if  ( pos == NULL )
            break;

        if  ( ( rand() % 2 ) == 1 ) 
            fputs( buffer, fout );
    }
    

    fclose( fin );
    fclose( fout );
}


/**
 * The main function is written so that the user can pass in the
 * following params:
 * 1. write_hex_mesh filename num_layers - generates a hexagonal mesh
 *    of num_layers layers of hexagonal cells, and writes it into
 *    filename. The algorithm is then run on the file.
 *
 * 2. write_random filename num_points - generates num_points
 *    different points randomly and writes it into filename. The
 *    algorithm is then run on the file.
 *
 * 3. filename - opens up filename assuming that the file contains
 *    valid coordinates of points. The algorithm is then run on the
 *    file.  The algorithm will use all points of the input. There is
 *    an option to random sample points from the input, b ut this is
 *    currently disabled. The limit may be changed in the function
 *    read_point_set. 5000 has been set for readability as very small
 *    voronoi cells are hard to see.  The details of how start and end
 *    are chosen are in algorithm::run, if different choices are
 *    desired.
 */
int  main( int  argc, char ** argv )
{
    Algorithm  alg;

    srand( time( NULL ) );
    setlocale(LC_ALL,""); // For nicer printf of numbers with commas

    if  ( ( argc == 4 )
          &&  ( strcasecmp( argv[ 1 ], "write_hex_mesh" ) == 0 ) ) {
        write_hex_mesh( argv[ 2 ], atoi( argv[ 3 ] ) );
        return  0;
    }
    if  ( ( argc == 4 )
          &&  ( strcasecmp( argv[ 1 ], "sparsify" ) == 0 ) ) {
        sparsify( argv[ 2 ], argv[ 3 ] );
        return  0;
    }
    if  ( ( argc == 4 )
          &&  ( strcasecmp( argv[ 1 ], "write_random" ) == 0 ) ) {
        write_random_mesh( argv[ 2 ], atoi( argv[ 3 ] ) );
        return  0;
    }
    if ( ( argc == 4 )
    &&  ( strcasecmp( argv[ 3 ], "full" ) == 0 ) ) {
        alg.setRandom(false);
        alg.read( argv[ 1 ] );
        alg.set_output_name_base( argv[ 2 ] );
        alg.run();
        return 0;
    }
    
    if  ( argc == 3 ) {
        alg.read( argv[ 1 ] );
        alg.set_output_name_base( argv[ 2 ] );
        alg.run();
        return 0;
    }

    usage();
    return  0;
}
 

////////////////////////////////////////////////////////////////////////////
// End of file
////////////////////////////////////////////////////////////////////////////

